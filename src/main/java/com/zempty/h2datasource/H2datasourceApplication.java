package com.zempty.h2datasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H2datasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(H2datasourceApplication.class, args);
    }

}
